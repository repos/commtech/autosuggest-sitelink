# Autosuggest linking Wikidata

A tool to suggest sitelinks between wiki pages and Wikidata items.

## Usage

See https://meta.wikimedia.org/wiki/Meta:AutosuggestSitelink#Usage

## Contributing

* Ensure you're using the Node version specified by [.nvmrc](.nvmrc)
* `npm install` to install dependencies
* `npm test` to run linting tests
* `npm run build` to build the compiled assets to the dist/ directory

While developing, you can load the compiled JavaScript served from your local to the wiki.
To do this, add the following to your [global.js](https://meta.wikimedia.org/wiki/Special:MyPage/global.js):

```
mw.loader.load('http://localhost:5501/dist/AutosuggestSitelink.js');
```

Then start the server with `node bin/server.js`.

You can use `npm run watch` to recompile the source files as changes are made to them.

The script is initialized with the `postEdit` hook in MediaWiki. To ease
development, you can set `window.AutosuggestSitelinkDebug = true;` in your
global.js before the `mw.loader.load()` line you just added. This will cause
AutosuggestSitelink to show dialog on page load, rather than requiring you
to first make an edit.

## Deployment

You must have `interface-admin` rights to use the deploy script.
Visit https://meta.wikimedia.org/wiki/Special:BotPasswords to obtain credentials,
then `cp credentials.json.dist credentials.json` and change the details accordingly:

```
{
   "username": "Exampleuser@somedescription",
   "password": "mybotpassword1234567890123456789"
}
```

To deploy, run `node bin/deploy.js "[edit summary]"`.
The edit summary is transformed to include the version number and git SHA, e.g. "v5.5.5 at abcd1234: [edit summary]".
