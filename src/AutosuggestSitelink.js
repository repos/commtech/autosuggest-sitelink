/**
 * @class
 * @property {string} dbName The db name of the current wiki
 * @property {string} page The name of page
 * @property {string} wikidataUrl
 * @property {string} infoUrl Documentation page for this script
 * @property {Object} api The api object to query data from wikidata
 */
class AutosuggestSitelink {

	constructor( $link = null ) {
		this.dbName = mw.config.get( 'wgDBname' );
		this.page = mw.config.get( 'wgPageName' );
		this.title = mw.config.get( 'wgTitle' );
		this.namespace = mw.config.get( 'wgNamespaceNumber' );
		this.wikidataUrl = 'https://www.wikidata.org';

		this.$toolboxLink = $link;

		// When on testwiki, use Test Wikidata.
		if ( this.dbName === 'testwiki' ) {
			this.wikidataUrl = 'https://test.wikidata.org';
		}

		this.api = new mw.ForeignApi( this.wikidataUrl + '/w/api.php' );

		this.windowManager = null;
		this.dialog = null;
	}

	/**
	 * Initialize AutosuggestSitelink by loading dependencies and translations
	 * and then opening the dialog window.
	 * @return {Promise}
	 */
	init() {
		if ( this.dialog && this.windowManager ) {
			return new Promise( () => {
				this.windowManager.openWindow( this.dialog );
				// Re-enable the toolbar link.
				this.$toolboxLink.css( 'color', '' );
			} );
		}
		return Promise.all( [
			// Resource loader modules
			mw.loader.using( [
				'mediawiki.action.view.postEdit',
				'mediawiki.ForeignApi',
				'mediawiki.Title',
				'oojs-ui-core',
				'oojs-ui-widgets',
				'oojs-ui-windows'
			] ),
			this.loadTranslations()
		] ).then( () => {
			const Dialog = require( './Dialog.js' );
			this.dialog = new Dialog( this );
			// Add the dialog to the window manager
			this.windowManager = OO.ui.getWindowManager();
			this.windowManager.addWindows( [ this.dialog ] );
			this.windowManager.openWindow( this.dialog );
			// Re-enable the toolbar link, and start the loading indicator in the dialog.
			this.$toolboxLink.css( 'color', '' );
			this.dialog.pushPending();
		} );
	}

	/**
	 * Check if this page already has a sitelink to this wiki,
	 * and if it does not, suggest possible matching items.
	 */
	checkSitelink() {
		if ( mw.config.get( 'wgWikibaseItemId' ) ) {
			// If there is already a sitelink to this page, just show a message.
			this.dialog.setMessage( 'success', mw.msg( 'asl-pagelinked' ) );
		} else {
			// Otherwise, suggest some Wikidata items to choose from.
			this.suggestSiteLink();
		}
		this.dialog.popPending();
	}

	/**
	 * Check whether a wikidata item is already
	 * connected to other sites.
	 *
	 * @param {string} title
	 * @return {jQuery.Promise}
	 */
	checkPreexistingSitelink( title ) {
		this.dialog.pushPending();
		const promise = new Promise( ( resolve ) => {
			this.api.get( {
				format: 'json',
				action: 'wbgetentities',
				props: 'sitelinks/urls',
				sitefilter: this.dbName,
				sites: this.dbName,
				ids: title
			} ).done( ( data ) => {
				this.dialog.popPending();
				if ( this.dbName in data.entities[ title ].sitelinks ) {
					resolve( true );
				}
				resolve( false );
			} );
		} );

		return promise;
	}

	/**
	 * Not found, include link to create new item.
	 *
	 * @return {jQuery.Object}
	 */
	itemsNotFound() {
		const $container = $( '<div>' );

		$container.append(
			$( '<p>' ).text( mw.msg( 'asl-notfound' ) )
		);

		return $container;
	}

	/**
	 * Find possible related wikidata items based on the page title.
	 * If items are found, a dialog is shown with a submission form.
	 */
	suggestSiteLink() {
		this.dialog.pushPending();
		this.api.get( {
			format: 'json',
			action: 'query',
			list: 'search',
			srsearch: this.title,
			srprop: 'snippet|titlesnippet'
		} ).then( ( data ) => {
			this.dialog.popPending();
			if ( data.query.searchinfo.totalhits === 0 ) {
				// No sitelinks at all.
				const itemsNotFoundNotification = '.mw-notification-area > #asl-itemsNotFound';
				if ( $( itemsNotFoundNotification ).length === 0 ) {
					this.$toolboxLink.css( 'color', '' );
					this.dialog.setMessage( 'warning', this.itemsNotFound() );
					this.dialog.toggleNewItemButton( true );
				}

				return;
			}

			const items = data.query.search;
			const promises = [];
			items.forEach( ( item ) => {
				const title = item.title;
				const preexisting = this.checkPreexistingSitelink( title );
				promises.push( preexisting );
				preexisting.then( ( result ) => {
					if ( result ) {
						item.hasSiteLink = true;
					} else {
						item.hasSiteLink = false;
					}
				} );
			} );
			Promise.all( promises ).then( () => {
				this.dialog.setItems( items );
				this.dialog.toggleNewItemButton( true );
				this.$toolboxLink.css( 'color', '' );
			} );
		} );
	}

	/**
	 * Send a POST request to add the sitelink.
	 *
	 * @param {string} item
	 * @return {jQuery.Promise}
	 */
	submit( item ) {
		this.dialog.pushPending();
		return this.api.postWithToken( 'csrf', {
			action: 'wbsetsitelink',
			id: item,
			linksite: this.dbName,
			linktitle: this.page
		} );
	}

	/**
	 * Load the translations from the on-wiki messages page.
	 *
	 * @return {jQuery.Deferred}
	 */
	loadTranslations() {
		const dfd = $.Deferred(),
			messagesPage = 'MediaWiki:Gadget-AutosuggestSitelink-messages',
			metaApi = new mw.ForeignApi( 'https://meta.wikimedia.org/w/api.php' ),
			userLang = mw.config.get( 'wgUserLanguage' ),
			langPageEn = `${ messagesPage }/en`,
			langPageLocal = `${ messagesPage }/${ userLang }`,
			titles = [ langPageEn ];
		if ( mw.config.get( 'wgUserLanguage' ) !== 'en' ) {
			// Fetch the translation in the user's language, if not English.
			titles.push( langPageLocal );
		}

		const coreMessagesPromise = metaApi.loadMessagesIfMissing( [
			'parentheses-start',
			'parentheses-end'
		] );
		const aslMessagesPromise = metaApi.get( {
			action: 'query',
			prop: 'revisions',
			titles,
			rvprop: 'content',
			rvslots: 'main',
			format: 'json',
			formatversion: 2
		} ).then( ( resp ) => {
			let messagesLocal = {},
				messagesEn = {};

			resp.query.pages.forEach( ( page ) => {
				let parsedContent;

				if ( page.missing ) {
					return;
				}

				/**
				 * The content model of the messages page is wikitext so that it can be used with
				 * Extension:Translate. Consequently, it's easy to break things, so we do a
				 * try/catch, try some commons fixes, and indicate the likely culprit to the user.
				 */
				let content = page.revisions[ 0 ].slots.main.content;
				try {
					try {
						parsedContent = JSON.parse( content );
					} catch {
						// A common failure reason is because of the HTML that's added for
						// untranslated messages, so remove this and try to parse again.
						// eslint-disable-next-line es-x/no-string-prototype-replaceall
						content = content.replaceAll( '<span lang="en" dir="ltr" class="mw-content-ltr">', '' );
						// eslint-disable-next-line es-x/no-string-prototype-replaceall
						content = content.replaceAll( '</span>', '' );
						parsedContent = JSON.parse( content );
					}
				} catch {
					// If it's still failing, there's something else wrong with the
					// messages (e.g. a double quote within a translated message).
					const metaLink = '<a href="https://meta.wikimedia.org/wiki/">' + page.title + '</a>';
					return OO.ui.alert(
						$(
							'<span>Unable to parse the messages page ' + metaLink + '. ' +
							'There may have been a recent change that contains invalid JSON.</span>'
						),
						{ title: 'Something went wrong' }
					);
				}

				if ( page.title === langPageLocal ) {
					messagesLocal = parsedContent.messages;
				} else {
					messagesEn = parsedContent.messages;
				}
			} );

			mw.messages.set( Object.assign( {}, messagesEn, messagesLocal ) );
		} );
		Promise.all( [ coreMessagesPromise, aslMessagesPromise ] ).then( () => {
			dfd.resolve();
		} );

		return dfd;
	}
}

let autosuggestSitelink = null;

function main( $link ) {
	$link.css( { color: '#C4C2C1' } );
	if ( !autosuggestSitelink ) {
		// Only instantiate the main object once.
		autosuggestSitelink = new AutosuggestSitelink( $link );
	}
	autosuggestSitelink.init().then(
		autosuggestSitelink.checkSitelink.bind( autosuggestSitelink )
	);
}
$.when( mw.loader.using( [ 'mediawiki.util' ] ), $.ready ).then( function () {
	// Return when not in a content namespace or the article does not exist.
	const nsIds = mw.config.get( 'wgNamespaceIds' );
	const validNamespaces = mw.config.get( 'wgContentNamespaces' )
		// Remove some namespaces that should never have sitelinks.
		.filter( ( e ) => {
			return e !== nsIds.file &&
				// Exclude Wikisource proofreading namespaces.
				e !== nsIds.page && e !== nsIds.index &&
				// Exclude Wiktionary mainspace.
				!( e === nsIds[ '' ] && mw.config.get( 'wgDBname' ).endsWith( 'wiktionary' ) ) &&
				// Exclude Wikidata's main, Property, and Lexeme namespaces.
				!( mw.config.get( 'wgDBname' ) === 'wikidatawiki' &&
					[ nsIds[ '' ], nsIds.lexeme, nsIds.property ].indexOf( e ) !== -1
				);
		} );
	// Add Category NS.
	validNamespaces.push( nsIds.category, nsIds.template );
	const ns = mw.config.get( 'wgNamespaceNumber' );
	if ( !validNamespaces.includes( ns ) || mw.config.get( 'wgArticleId' ) === 0 ) {
		return;
	}

	// Add sidebar link.
	const $link = $( mw.util.addPortletLink( 'p-tb', '#', 'AutosuggestSiteLink', 'tb-autosuggestsitelink', null, null, '#t-whatlinkshere' )
		.querySelector( 'a' ) );
	$link.on( 'click', ( e ) => {
		main( $link );
		e.preventDefault();
	} );

	// Add postEdit hook on pages that are not already linked.
	if ( !mw.config.get( 'wgWikibaseItemId' ) ) {
		mw.hook( 'postEdit' ).add( main.bind( this, $link ) );
	}
} );
