/**
 * @class
 * @property {AutosuggestSitelink} aslContext
 * @property {Array<Object>} items
 */
class Dialog extends OO.ui.ProcessDialog {

	static name = 'aslDialog';

	static size = 'large';

	/**
	 * @param {AutosuggestSitelink} aslContext
	 * @param {Array<Object>} items
	 * @constructor
	 */
	constructor( aslContext ) {
		super();
		this.aslContext = aslContext;
		this.helpUrl = 'https://meta.wikimedia.org/wiki/Meta:AutosuggestSitelink';

		// These properties are set here instead of using the static keyword above
		//   because mw.msg() isn't available in the static context.
		Dialog.title = mw.msg( 'asl-popuptitle' );
		Dialog.actions = [
			{
				action: 'submit',
				label: mw.msg( 'asl-submit' ),
				flags: [ 'primary', 'progressive' ],
				disabled: true
			},
			{
				action: 'help',
				label: mw.msg( 'asl-help' ),
				icon: 'helpNotice',
				href: this.helpUrl
			},
			{
				action: 'close',
				flags: [ 'safe', 'close' ]
			}
		];

		// Add properties needed by the ES5-based OOUI inheritance mechanism.
		// This roughly simulates OO.inheritClass()
		Dialog.parent = Dialog.super = OO.ui.ProcessDialog;
		OO.initClass( OO.ui.ProcessDialog );
		Dialog.static = Object.create( OO.ui.ProcessDialog.static );
		Object.keys( Dialog ).forEach( ( key ) => {
			Dialog.static[ key ] = Dialog[ key ];
		} );
	}

	initialize() {
		super.initialize();

		// Message widget.
		this.message = new OO.ui.MessageWidget();
		this.message.toggle( false );

		// Search results list.
		this.itemSelect = new OO.ui.RadioSelectWidget()
			.connect( this, {
				choose: () => this.getActions().setAbilities( { submit: true } )
			} );
		const description = new OO.ui.Element( {
			$content: $( '<p>' ).text( mw.msg( 'asl-heading-desc' ) )
		} );
		this.itemsFieldset = new OO.ui.FieldsetLayout( {
			label: mw.msg( 'asl-heading' ),
			items: [ description, this.itemSelect ]
		} );
		this.itemsFieldset.toggle( false );

		// New item button.
		const newItemUrl = this.aslContext.wikidataUrl + '/wiki/Special:NewItem?' +
			new URLSearchParams( {
				site: this.aslContext.dbName,
				page: this.aslContext.page,
				label: this.aslContext.title,
				lang: mw.config.get( 'wgContentLanguage' )
			} );
		this.newItemButton = new OO.ui.ButtonWidget( {
			href: newItemUrl,
			label: mw.msg( 'asl-createnewitem' ),
			target: '_blank',
			flags: [ 'progressive' ]
		} );
		this.newItemButton.toggle( false );

		// Put them all together in a panel.
		const panel = new OO.ui.PanelLayout( { padded: true, expanded: false } );
		panel.$element.append(
			this.message.$element,
			this.itemsFieldset.$element,
			$( '<p>' ).append( this.newItemButton.$element )
		);
		this.$body.append( panel.$element );
	}

	/**
	 * Set items for the dialog
	 *
	 * @param {Object} items
	 */
	setItems( items ) {
		this.itemSelect.clearItems();
		this.itemSelect.addItems( this.#getItemsFieldset( items ) );
		this.itemsFieldset.toggle( true );
	}

	/**
	 * Get the height of the dialog's body.
	 *
	 * @return {number}
	 */
	getBodyHeight = function () {
		return 250;
	};

	setMessage( type, label ) {
		this.message.setType( type );
		this.message.setLabel( label );
		this.message.toggle( true );
	}

	/**
	 * Toggle the display of the new-item button.
	 *
	 * @param {boolean} show Whether to hide or show the button.
	 */
	toggleNewItemButton( show ) {
		this.newItemButton.toggle( show );
	}

	/**
	 * Get the radio button options for the given items.
	 *
	 * @param {Array<Object>} items
	 * @return {Array<OO.ui.RadioOptionWidget>}
	 */
	#getItemsFieldset( items ) {

		/**
		 * Quick helper function to strip out HTML from a string,
		 * which is present in the search result snippets because
		 * HTML is used to highlight the searched term.
		 *
		 * @param {string} str
		 * @return {string}
		 */
		const stripHTML = ( str ) => str.replace( /<\/?.+?>/ig, '' );

		const itemOptions = [];
		items.forEach( ( item ) => {
			const $qItemLink = $( '<a>' )
				.attr( 'href', this.aslContext.wikidataUrl + '/wiki/' + item.title )
				.attr( 'target', '_blank' )
				.text( item.title );
			const $qItemLabel = $( '<span>' ).text(
				mw.msg( 'parentheses-start' ) +
				stripHTML( item.titlesnippet ) +
				mw.msg( 'parentheses-end' )
			);

			const $qItemDesc = $( '<span>' )
				.attr( 'style', 'font-style: italic; display: block; padding-right: 5px;' )
				.text( stripHTML( item.snippet ) || mw.msg( 'asl-nodescription' ) );
			// Check if the item has a sitelink
			if ( item.hasSiteLink ) {
				const $qItemWarning = $( '<span>' )
					.text( mw.msg( 'asl-alreadylinked' ) )
					.attr( 'style', 'font-weight: bold; display: block;' );
				$qItemDesc.append( $qItemWarning );
			}
			itemOptions.push(
				new OO.ui.RadioOptionWidget( {
					label: $( '<span>' ).append( $qItemLink, ' ', $qItemLabel, $qItemDesc ),
					data: item.title
				} )
			);
		} );
		return itemOptions;
	}

	/**
	 * @param {string} action
	 * @return {OO.ui.Process}
	 * @override
	 */
	getActionProcess( action ) {
		return super.getActionProcess( action )
			.next( () => {
				if ( action === 'submit' ) {
					return this.aslContext.submit( this.itemSelect.findSelectedItem().data );
				}
				if ( action === 'help' ) {
					window.open( this.helpUrl );
				}

				return super.getActionProcess( action );
			} )
			.next( () => {
				if ( action === 'submit' ) {
					this.close( { action } );
					// Show a message, purge the page, and then reload the page
					// so that any dependencies (including AutosuggestSitelink)
					// are updated with the new sitelink.
					mw.notify( mw.msg( 'asl-itemlinked' ), {
						autoHideSeconds: 'long'
					} );
					( new mw.Api() )
						.post( { action: 'purge', titles: mw.config.get( 'wgPageName' ) } )
						.then( window.location.reload );
				}

				return super.getActionProcess( action );
			} )
			.next( () => {
				if ( action === 'close' ) {
					this.close( { action } );
				}

				return super.getActionProcess( action );
			} );
	}

}
module.exports = Dialog;
